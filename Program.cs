using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.src.Data;

var builder = WebApplication.CreateBuilder(args);


//builder.Services.AddDbContext<PaymentApiContext>(options =>
  //  options.UseSqlServer(builder.Configuration.GetConnectionString("ConexaoPadrao")));

builder.Services.AddDbContext<PaymentApiContext>(opt =>
    opt.UseInMemoryDatabase("PaymentApi"));


builder.Services.AddControllers();

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());


builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
