using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using tech_test_payment_api.src.Entidades;

namespace tech_test_payment_api.src.Data.Dtos
{
    public class ReadVendaDto
    {
        
        [Key()]
        public int VendaId { get; set; }

       
        public DateTime DataVenda { get; set; }

        
        [ForeignKey("Vendedor")]
        public int VendedorId { get; set; }

   
        public virtual Vendedor Vendedor { get; set; }

        [ForeignKey("Pedido")]
       public int PedidoId { get; set; }

        public virtual Pedido Pedido { get; set; }


        [JsonConverter(typeof(JsonStringEnumConverter))]
        public StatusCompra Status { get; set; }
    }
}