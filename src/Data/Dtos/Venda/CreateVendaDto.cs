using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using tech_test_payment_api.src.Entidades;

namespace tech_test_payment_api.src.Data.Dtos
{
    public class CreateVendaDto
    {
        

       
        public DateTime DataVenda { get; set; }

        
        [ForeignKey("Vendedor")]
        [JsonIgnore]
        public int VendedorId { get; set; }
        public virtual Vendedor Vendedor { get; set; }

        [ForeignKey("Pedido")]
        [JsonIgnore]
        public int PedidoId { get; set; }       
        public virtual Pedido Pedido { get; set; }


        
    }
}