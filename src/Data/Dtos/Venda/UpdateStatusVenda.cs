using System.Text.Json.Serialization;
using tech_test_payment_api.src.Entidades;

namespace tech_test_payment_api.src.Data.Dtos
{
    public class UpdateStatusVenda
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public StatusCompra Status { get; set; }

       
        
    }
}