using AutoMapper;
using tech_test_payment_api.src.Data.Dtos;
using tech_test_payment_api.src.Entidades;

namespace tech_test_payment_api.src.Profiles
{
    public class VendaProfile : Profile
    {
        public VendaProfile()
        {
            CreateMap<CreateVendaDto, Venda>();
            CreateMap<Venda, ReadVendaDto>();
            //CreateMap<>();

        }
    }
}