using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.src.Data;
using tech_test_payment_api.src.Data.Dtos;
using tech_test_payment_api.src.Entidades;

namespace tech_test_payment_api.src.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PaymentApiContext _context;
        private readonly IMapper _mapper;

        public VendaController(PaymentApiContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        [HttpPost("AdicionarVenda")]
        public IActionResult AdicionarVenda([FromBody] CreateVendaDto vendaDto)
        {
            Venda venda = _mapper.Map<Venda>(vendaDto);
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return Ok();
        }

        [HttpGet("ListarTodasVendas")]
        public IEnumerable<Venda> ListarTodasVendas()
        {
            return _context.Vendas;

        }

        [HttpGet("ObterVendaPorId")]
        public IActionResult ObteerVendaPorId([FromQuery]int id)
        {
            Venda venda = _context.Vendas.FirstOrDefault(venda => venda.VendaId == id);

            if(venda != null)
            {
                ReadVendaDto vendaDto = _mapper.Map<ReadVendaDto>(venda);
                return Ok(vendaDto);
                

            }

            return NotFound();

        }

        [HttpPut("AtualizarStatusCompra")]
        public IActionResult AtualizarStatusCompra([FromQuery]int id, [FromBody]UpdateStatusVenda vendaDto)
        {
            //var atualizacaoVenda = vendaDto;
            Venda venda = _context.Vendas.FirstOrDefault(venda => venda.VendaId == id);
            if(venda != null)
            {
                //var statusVenda = venda.Status;
                switch(venda.Status)
                {
                    case StatusCompra.Aguardando_pagamento:
                        if(vendaDto.Status == StatusCompra.Pagamento_aprovado || vendaDto.Status == StatusCompra.Cancelada){

                            venda.Status = vendaDto.Status;

                            _context.Vendas.Attach(venda);
                            _context.Entry(venda).Property(x => x.Status).IsModified = true;
                            _context.SaveChanges();

                            return Ok(venda);

                        }
                        break;

                    case StatusCompra.Pagamento_aprovado:
                        if (vendaDto.Status == StatusCompra.Enviado_para_transportadora || vendaDto.Status == StatusCompra.Cancelada)
                        {

                            venda.Status = vendaDto.Status;

                            _context.Vendas.Attach(venda);
                            _context.Entry(venda).Property(x => x.Status).IsModified = true;
                            _context.SaveChanges();

                            return Ok(venda);
                        }
                        break;

                    case StatusCompra.Enviado_para_transportadora:
                        if (vendaDto.Status == StatusCompra.Entregue)
                        {

                            venda.Status = vendaDto.Status;

                            _context.Vendas.Attach(venda);
                            _context.Entry(venda).Property(x => x.Status).IsModified = true;
                            _context.SaveChanges();

                            return Ok(venda);
                        }
                        break;
                }
            }

            return NotFound();
        }

        [HttpDelete("DeletarVenda")]
        public IActionResult DeletaVenda([FromQuery]int id)
        {
            Venda venda = _context.Vendas.FirstOrDefault(venda => venda.VendaId == id);

            if (venda == null)
            {
                return NotFound();
            }

            _context.Remove(venda);
            _context.SaveChanges();

            return NoContent();

        }


        
    }
}