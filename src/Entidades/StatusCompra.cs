using System.Text.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace tech_test_payment_api.src.Entidades
{
    [Flags]
    
     
    public enum StatusCompra 
    {
        Aguardando_pagamento = 1,
        Pagamento_aprovado = 2,
        Enviado_para_transportadora = 3,
        Entregue = 4,
        Cancelada = 5
    }
  
        
    
}