using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.src.Entidades
{
    public class Item
    {
         [Key()]
         [Required]
        public int ItemId { get; set; }

        public string Nome { get; set; }

        public double Valor { get; set; }
        
    }
}