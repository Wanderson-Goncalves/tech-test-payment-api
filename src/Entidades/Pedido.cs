using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.src.Entidades
{
    public class Pedido
    {
        [Key]
        [Required]
        public int PedidoId { get; set; }

        public virtual ICollection<Item> Itens { get; set; }

    }
}