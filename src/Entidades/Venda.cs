using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace tech_test_payment_api.src.Entidades
{
    public class Venda
    {
        public Venda()
        {


        }


        [Key()]
        [Required]
        public int VendaId { get; set; }

        public DateTime DataVenda { get; set; }


        [ForeignKey("Vendedor")]
        public int VendedorId { get; set; }
        public virtual Vendedor Vendedor { get; set; }

        [ForeignKey("Pedido")]
        public int PedidoId { get; set; }
        public virtual Pedido Pedido { get; set; }

        public StatusCompra Status { get; set; } = StatusCompra.Aguardando_pagamento;




    }
}