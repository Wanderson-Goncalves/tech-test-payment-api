using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.src.Entidades
{
    public class Vendedor
    {
        [Key()]
        [Required]        
        public int VendedorId { get; set; }
        public string Nome { get; set; }

        public string Cpf { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        [JsonIgnore]
        public virtual ICollection<Venda> Vendas { get; set; }

    }
}